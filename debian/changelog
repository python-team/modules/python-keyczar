python-keyczar (0.716+ds-4) UNRELEASED; urgency=medium

  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Ondřej Nový <onovy@debian.org>  Sat, 20 Jul 2019 00:07:37 +0200

python-keyczar (0.716+ds-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/tests: Use AUTOPKGTEST_TMP instead of ADTTMP
  * d/control: Remove ancient X-Python-Version field

  [ Chris Lamb ]
  * Remove trailing whitespaces from d/changelog

  [ Christian Kastner ]
  * d/control:
    - Bump Standards-Version to 4.3.0 (no changes needed)
    - Switch Build-Depends from debhelper to debhelper-compat
    - Bump Build-Depends for debhelper-compat to 12
    - Add Rules-Requires-Root: no
      We don't need (fake)root for building the package
  * Drop d/compat, as made obsolete by debhelper-compat
  * d/source/lintian-overrides:
    - Add tag debian-rules-contains-unnecessary-get-orig-source-target
      We can't use pure uscan, as we move around directories during repacking
  * Add d/NEWS to alert of the plan to remove this package soon
  * Add d/gbp.conf for DEP-14 branch names

 -- Christian Kastner <ckk@debian.org>  Fri, 15 Feb 2019 21:05:07 +0100

python-keyczar (0.716+ds-2) unstable; urgency=medium

  * Team upload

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Fix Format URL to correct one

  [ Pierre-Elliott Bécue ]
  * Get rid of git-dpm
  * d/changelog stripped from whitespaces
  * d/rules:
    - Remove the dh_clean rule
    - Replace the dpkg-parsechangelog by the use of DEB_VERSION
  * d/control:
    - Bump debhelper Build-Dependency to version 11
    - Bump Standards-Version to 4.1.3
  * Bump dh compat to 11
  * d/s/lintian-overrides:
    - Rename one lintian tag
    - Add override for the absence of python3 package
  * d/watch to version 4
  * d/copyright:
    - Add missing copyright statements for two files
    - Add comment to explain the +ds in the version of the package
  * Add patch d/p/0001 to fix an issue that appeared with a new pyasn1
    release. (Closes: #886115)

 -- Pierre-Elliott Bécue <becue@crans.org>  Wed, 21 Feb 2018 07:58:19 +0100

python-keyczar (0.716+ds-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Bump Build-Depends version of python-crypto to (>= 2.1)
  * d/copyright:
    - Update Upstream-Contact field
    - Bump copyright years

 -- Christian Kastner <ckk@debian.org>  Sun, 22 May 2016 23:16:31 +0200

python-keyczar (0.715+ds-4) unstable; urgency=medium

  * d/control:
    - Bump Standards-Version to 3.9.8 (no changes needed)

 -- Christian Kastner <ckk@debian.org>  Sun, 17 Apr 2016 16:17:36 +0200

python-keyczar (0.715+ds-3) unstable; urgency=medium

  [ SVN-Git Migration ]
  * git-dpm config
  * d/control:
    - Update Vcs fields for git migration

  [ Christian Kastner ]
  * d/control:
    - Bump Standards-Version to 3.9.7 (no changes needed)
    - Switch Vcs-Git from insecure URI to secure one

 -- Christian Kastner <ckk@debian.org>  Mon, 21 Mar 2016 13:16:41 +0100

python-keyczar (0.715+ds-2) unstable; urgency=medium

  * d/control:
    - Switch Maintainer email to my @debian.org address
    - Update Homepage field to new location at GitHub
  * d/copyright:
    - Update Source field to new location at GitHub

 -- Christian Kastner <ckk@debian.org>  Wed, 23 Sep 2015 00:00:26 +0200

python-keyczar (0.715+ds-1) unstable; urgency=medium

  * New upstream release.
    - Source is repacked from now on, as the original source tarball contains
      the source for the Java and C++ implementations of keyczar as well
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes needed)
    - Drop XS-Testsuite, as dpkg now recognizes the Testsuite header, and
      dpkg-source automatically adds one when an autopkgtest suite is found
  * debian/rules:
    - Drop return code checking, pybuild checks the result for us
    - Add a get-orig-source target
  * debian/watch:
    - Upstream moved to GitHub

 -- Christian Kastner <debian@kvr.at>  Mon, 06 Jul 2015 22:00:28 +0200

python-keyczar (0.71d-09062013-2) unstable; urgency=low

  * debian/control:
    - Add Build-Depends for dh-python (for pybuild)
    - Lowercase package synopsis
  * debian/rules:
    - Convert to use pybuild build system
  * debian/tests/unittests-default:
    - Refresh comments

 -- Christian Kastner <debian@kvr.at>  Sat, 09 Aug 2014 21:29:15 +0200

python-keyczar (0.71d-09062013-1) unstable; urgency=low

  [ Jakub Wilk ]
  * Remove DM-Upload-Allowed; it's no longer used by the archive
    software.
  * Fix a typo in the package description.

  [ Christian Kastner ]
  * New upstream release. Closes: #699872
    - Expose the CLI tool /usr/bin/keyczart
  * debian/copyright:
    - Update to machine-readable format v1.0
    - Refresh copyright years
  * debian/control:
    - Bump Standards-Version to 3.9.5 (no changes needed)
    - Bump debhelper dependency to (>= 9)
    - Drop dependency on python-support and bump python-all dependency
      to (>= 2.6.6-3~) for dh_python2
    - Add X-Python-Version with >= 2.6, thereby implicitly dropping support for
      Python 2.5(it used to be supported in the now removed debian/pyversions).
      This allows us to simplify some things, such as:
    - Drop the Build-Depends for python (>= 2.6.6-14~) | pythons-simplejson.
      This was only needed to ensure that either Python 2.5 was not installed
      or else that python-simplejson was installed.
    - Add Build-Depends for python-setuptools. Upstream switch build mechanism
    - Add XS-Testsuite: autopkgtest
  * debian/compat:
    - Bump debhelper compatibility level to 9
  * debian/rules:
    - Drop the get-orig-source target (no longer needed)
    - Convert build process to dh_python2
    - Drop unit test running for Python << 2.6 (no longer supported)
    - Remove the egg-info directory during clean
    - Upstream moved location of unit tests
  * debian/svn-orig-source.sh
    - Drop (no longer needed)
  * debian/pyversions:
    - Remove (obsolete)
  * debian/tests/*:
    - Create an autopkgtest which executes the unit tests using the default
      Python interpreter
  * debian/watch:
    - Update Google Code URLs following wiki.debian.org recommendations
      (apparently, the URLs are stable now, and no longer require a helper)
    - The one-letter suffix of the upstream version number really is part of
      the version number (ie, the previous "b" was not a "beta" qualifier)
    - Explicitly fix one-time broken version number 06b to 0.6b. Upstream
      otherwise follows the MAJOR.MINOR(letter) release pattern.
  * debian/source/lintian-overrides:
    - Add override for debian-watch-may-check-gpg-signature
  * debian/manpages
    debian/keyczart.1
    - Create manpage for the keyczart tool

 -- Christian Kastner <debian@kvr.at>  Fri, 07 Mar 2014 14:17:37 +0100

python-keyczar (0.6~b.061709+svn502-1) unstable; urgency=low

  [ Christian Kastner ]
  * Grab newest version from SVN. The diff to 0.6~b.061709 is just a few
    commits and comprises mainly bugfixes. Closes: #627727
  * debian/control:
    - Bumped Standards-Version to 3.9.2 (no changes needed)
  * debian/svn-orig-source.sh:
    - Added this script which creates an upstream tarball from an SVN revision
  * debian/rules:
    - Added get-orig-source target (via svn-get-orig.sh above)
  * debian/patches dropped:
    - 0001-use-os-urandom-for-entropy
      No longer needed, the current SVN snapshot includes it

  [ Jakub Wilk ]
  * Add DM-Upload-Allowed.

 -- Christian Kastner <debian@kvr.at>  Thu, 26 May 2011 23:49:27 +0200

python-keyczar (0.6~b.061709-3) unstable; urgency=low

  * Team upload.

  [ Christian Kastner ]
  * debian/control:
    - Bump Standards-Version to 3.9.1 (no changes needed)

  [ Luke Faraone ]
  * debian/rules:
    - Handle cases when Python 2.5 is not used during build. Fixes FTBFS when
      running tests. Closes: #601259
  * debian/patches:
    - Remove 0001-workaround-deprecated-interface
      This patch did not work with older versions of PyCrypto. Closes: #601264
    - Added 0001-use-os-urandom-for-entropy
      This cherry-picked patch uses os.urandom() directly for entropy.

 -- Luke Faraone <lfaraone@debian.org>  Mon, 08 Nov 2010 22:29:17 -0500

python-keyczar (0.6~b.061709-2) unstable; urgency=low

  * debian/copyright:
    - Add missing copyright information for PyCrypto documentation included in
      source package. Closes: #588704
  * debian/patches:
    - Added 0001-workaround-deprecated-interface
      This resolves the DeprecationWarning printed by PyCrypto.

 -- Christian Kastner <debian@kvr.at>  Sat, 17 Jul 2010 01:04:30 +0200

python-keyczar (0.6~b.061709-1) unstable; urgency=low

  * Initial release (Closes: #494868)

 -- Christian Kastner <debian@kvr.at>  Fri, 02 Jul 2010 22:49:40 +0200
